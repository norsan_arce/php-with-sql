let firstName = document.getElementById("firstName");
let lastName = document.getElementById("lastName");
let email = document.getElementById("email");
let password = document.getElementById("password");
let confirmPassword = document.getElementById("confirmPassword");
let registerBtn = document.getElementById("registerUser");

const validate = (firstName, lastName, email, password, confirmPassword) => {

let errors = 0;

	if(firstName.value ==""){
		firstName.nextElementSibling.textContent = "Please provide a valid first name.";
		errors++
	}else{
		firstName.nextElementSibling.textContent = "";
	}

	if(lastName.value == ""){
		lastName.nextElementSibling.textContent = "Please provide a valid last name.";
	}else{
		lastName.nextElementSibling.textContent = "";
	}

	if(email.value == ""){
		email.nextElementSibling.textContent = "Please provide a valid E-mail.";
	}else{
		email.nextElementSibling.textContent = "";
	}

	if(password.value == ""){
		password.nextElementSibling.textContent = "Please provide a valid password.";
	}else{
		password.nextElementSibling.textContent = "";
	}

	if(confirmPassword.value == ""){
		confirmPassword.nextElementSibling.textContent = "Please provide a password similar to the one above.";
	}else{
		confirmPassword.nextElementSibling.textContent = "";
	}

	if(errors>0){
	return false;
	}else{
	return true;
	}
	}

	confirmPassword.addEventListener("input", ()=>{
	// if(password.value.length < 8 && password.value.length > 24){
	// password.nextElementSibling.textContent = "Password must be between 8-24 characters.";
	// }else{
	// password.nextElementSibling.textContent = "";
	// }

	if(password.value != confirmPassword.value){
	confirmPassword.nextElementSibling.textContent = "Passwords do not match";
	}else{
	confirmPassword.nextElementSibling.textContent = "";
	}
	});

	registerBtn.addEventListener("click", ()=>{
	if(validate(firstName, lastName, email, password, confirmPassword)){
	let data = new FormData;

	data.append("firstName", firstName.value);
	data.append("lastName", lastName.value);
	data.append("email", email.value);

	fetch("../Controllers/register-process.php",{
	method: "POST",
	body: data
	}).then(res=>res.text())
	.then(res=>{
	if(res == "user_exists"){
	email.nextElementSibling.textContent = "User already exists."
	}else{
	window.location.replace("login.php")
	}
	})
}	
})
