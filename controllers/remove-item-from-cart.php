<?php
	session_start();

	$removeItem = $_GET['id'];

	unset($_SESSION['cart'][$removeItem]);

	header("Location: ". $_SERVER['HTTP_REFERER']);
?>