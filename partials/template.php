<!DOCTYPE html>
<html>
<head>
	<title><?php get_title(); ?></title>
<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/yeti/bootstrap.css">
	<style type="text/css">
		img{
			object-fit: contain;
			object-position: center;
		}
	</style>
</head>
<body>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
		  <a class="navbar-brand" href="../index.php">Lapse Top</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03" aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor03">
		    <ul class="navbar-nav mr-auto ml-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="../views/catalog.php">Watches Menu<span class="sr-only">(current)</span></a>
		      </li>
		      	<li class="nav-item">
		        <a class="nav-link" href="add-item.php">Add Item</a>
		      	</li>
		       <li class="nav-item">
		       <a class="nav-link" href="cart.php">Cart:<span class="badge bg-light" id="cartCount">
		       	<?php 
		       	session_start();
		       	if(isset($_SESSION['cart'])){
		       		echo array_sum($_SESSION['cart']);
		       	} else {
		       		echo 0;
		       	}

		       	 ?>

		       </span></a>
		       </li>	
		       <li class="nav-item">
		        <a class="nav-link" href="#">Hello!</a>
		    	</li>
		    	<li class="nav-item">
		        <a class="nav-link" href="../controllers/logout-process.php">Logout</a>
		    	</li>
 				  <li class="nav-item">
		      	  	<a class="nav-link" href="login.php">Login</a>
		      	  </li>
		      	  <li class="nav-item">
		      	  	<a class="nav-link" href="register.php">Register</a>
		      	  </li>	    
		      	</ul>
		  </div>
		</nav>

		<!-- Page Contents -->
<?php 
 get_body_contents()
?>		



		
		<!-- Footer -->
		<footer class="page-footer font-small bg-light navbar-dark">
			<div class="footer-copyright text-center py-3">All rights reserved. 2020</div>
		</footer>


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>