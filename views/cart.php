<?php 
	require "../partials/template.php";

	function get_title(){
		echo "Cart";
	}

	function get_body_contents(){
		require "../controllers/connection.php";

?>
<h1 class="text-center py-5">CART PAGE</h1>
<hr>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped table-bordered">
		<thead>
			<tr class="text-center">
				<th>Item</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Subtotal</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$total = 0;

				// var_dump($_SESSION);

				if(isset($_SESSION['cart'])){
					foreach($_SESSION['cart'] as $itemId => $quantity) {
						$item_query = "SELECT * FROM items WHERE id = $itemId";


						$indiv_item = mysqli_fetch_assoc(mysqli_query($conn,$item_query));


						$subtotal = $indiv_item['price'] *$quantity;

						$total += $subtotal;

						?>
						<tr>
							<td><?php echo $indiv_item['name'] ?></td>
							<td><?php echo $indiv_item['price'] ?></td>
							<td>
                                                <span class="spanQ"><?php echo $quantity ?></span>
                                                <form action="../controllers/add-to-cart-process.php" method="POST" class="d-none">
                                                        <input type="hidden" name="id" value="<?php echo $itemId ?>">
                                                        <input type="hidden" name="fromCartPage" value="fromCartPage">
                                                        <input type="number" class="form-control" name="cart" value="<?php  echo $quantity ?>" data-id="<?php echo $itemId ?>">

                                                </form>
                                        </td>
							<td><?php echo number_format($subtotal,2) ?></td>
							<td>
								<a href="../controllers/remove-item-from-cart.php?id=<?php echo $itemId?> " class="btn-sm btn-danger">Remove</a>

							</td>
						</tr>
							
						
						<?php
					}
				}

			 ?>
			 <tr>
			 	<td></td>
			 	<td></td>
			 	<td></td>
			 	<td>Total:</td>
			 	<td><?php echo number_format($total,2) ?></td>
			 	
			 </tr>
		</tbody>
	</table>
	<div class="text-center">
	<a href="../controllers/empty-cart-process.php" class="btn btn-danger" type="submit"> Empty</a>
	</div>
</div>
<script type="text/javascript" src="../assets/scripts/update-cart.js"></script>

<?php 
}
 ?>




